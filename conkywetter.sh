#! /bin/bash
username=$(who -q)

if [[ $username == *martin* ]]; then
    cd ~/pyvenvs/python3/conkywetter
    source bin/activate

    # virtualenv is now active, which means your PATH has been modified and python
    # can be called like this.

    cd ~/workspace/conkywetter

    python conkywetter.py

    deactivate

    # a sample crontab entry:
    # to check for jobs by the loged in user: crontab -l
    # to edit/create the jobs for the loged in user: crontab -e
    # entry: 0,30 * * * * /home/martin/workspace/conkywetter/conkywetter/conkywetter.sh
    # entry: 15,45 * * * * /home/martin/workspace/conkywetter/conkywetter/conkywetter.sh
    # entry: @reboot sleep 60 && /home/martin/workspace/conkywetter/conkywetter/conkywetter.sh (doesn't work with encrypted home directory)
    #
    # Conky with delayed start:
    # conky -o -p 15 -d
    # Second conky:
    # conky -o -c /home/martin/.conkyrc-todo -p 20 -d
fi
