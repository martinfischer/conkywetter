"""
conkywetter.py holt Wetterdaten und erstellt damit ein Bild, das dann mittels
conky am Desktop angezeigt wird.
"""
import os
import re
import requests
from time import sleep
from datetime import datetime
from bs4 import BeautifulSoup
from PIL import Image, ImageDraw, ImageFont
from spritepositions import spritepositions


url_current = "http://wetter.orf.at/tirol/kirchdorf/#tag"
url_forecast = "http://wetter.orf.at/tirol/prognose"


def write_log(message):
    """
    Schreibt ein Logfile
    """
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d %H:%M")
    programm_filename = os.path.basename(__file__)
    message = "{:20}-->  {}  -->  {}\n".format(programm_filename, timestamp, message)
    logfile = os.path.expanduser("~/.applog")
    with open(logfile, "a") as f:
        f.write(message)


def get_weather_current():
    """
    Holt die aktuellen Wetterdaten von url_current
    """
    r = requests.get(url_current)
    html = r.text
    soup = BeautifulSoup(html)

    data = {}

    data["wetter"] = soup.find_all(class_="weatherIcon")[0]["title"]

    data["icon_list"] = soup.find_all(class_="weatherIcon")[0]["class"]

    datatable = soup.find_all(class_="dataTable")[0].p.next_siblings
    datatablelist = list(datatable)
    data["temp"] = "{} °C".format(datatablelist[3].get_text().split()[1])
    data["wind"] = "{} {} km/h".format(
                                    datatablelist[7].get_text().split()[1],
                                    datatablelist[7].get_text().split()[2])

    regex = re.compile("Luftfeuchtigkeit")
    data["luftfeuchtigkeit"] = "{} %".format(
                soup.find("span", text=regex).parent.get_text().split()[1])

    return data



def get_weather_forecast():
    """
    Holt die Wettervorhersagedaten von url_forecast
    """
    r = requests.get(url_forecast)
    html = r.text
    soup = BeautifulSoup(html)

    day1 = {}
    day2 = {}
    day3 = {}

    day1["wochentag"] = soup.find_all("th", "dayCol")[0].get_text().split()[0]
    day2["wochentag"] = soup.find_all("th", "dayCol")[1].get_text().split()[0]
    day3["wochentag"] = soup.find_all("th", "dayCol")[2].get_text().split()[0]

    day1["wetter"] = soup.find_all(class_="weatherIcon")[0].get_text()
    day2["wetter"] = soup.find_all(class_="weatherIcon")[1].get_text()
    day3["wetter"] = soup.find_all(class_="weatherIcon")[2].get_text()

    day1["icon_list"] = soup.find_all(class_="weatherIcon")[0]["class"]
    day2["icon_list"] = soup.find_all(class_="weatherIcon")[1]["class"]
    day3["icon_list"] = soup.find_all(class_="weatherIcon")[2]["class"]

    day1["temp_morning"] = "{} °C".format(
                    soup.find_all(class_="morning")[0].get_text().split()[0])
    day2["temp_morning"] = "{} °C".format(
                    soup.find_all(class_="morning")[1].get_text().split()[0])
    day3["temp_morning"] = "{} °C".format(
                    soup.find_all(class_="morning")[2].get_text().split()[0])

    day1["temp_highest"] = "{} °C".format(
                    soup.find_all(class_="highest")[0].get_text().split()[0])
    day2["temp_highest"] = "{} °C".format(
                    soup.find_all(class_="highest")[1].get_text().split()[0])
    day3["temp_highest"] = "{} °C".format(
                    soup.find_all(class_="highest")[2].get_text().split()[0])

    data = {}
    data["day1"] = day1
    data["day2"] = day2
    data["day3"] = day3

    return data


def get_icon_location(icon_name):
    """
    Holt die Koordinaten des jeweiligen Wettericons auf dem Sprite
    """
    if icon_name.startswith("c") or icon_name.startswith("night-"):
        return (spritepositions[icon_name][0],
                spritepositions[icon_name][1],
                spritepositions[icon_name][0] + 55,
                spritepositions[icon_name][1] + 55)
    else:
        return False


def draw_image(current_data, forecast_data):
    """
    Setzt das Bild zusammen
    """
    # Das Sprite mit den Icons
    weathericons_file = "img/weathericons.png"
    weathericons = Image.open(weathericons_file)

    # Die transparente Vorlage
    conkywetterstart_file = "img/conkywetterstart.png"
    conkywetterstart = Image.open(conkywetterstart_file)

    # Schriftfarbe
    color = (200, 200, 200, 200)

    # Schrift und Schriftgrösse
    font = ImageFont.truetype("font/Ubuntu-R.ttf", 14)

    # Schrift und Schriftgrösse - Temperatur
    temp_now_font = ImageFont.truetype("font/Ubuntu-R.ttf", 42)

    # Das draw Object
    draw = ImageDraw.Draw(conkywetterstart)

    # Jetziges Wetter - Icon oben links
    now = datetime.now()
    hour = int(now.strftime("%H"))
    if hour > 18 or hour < 7:
        now_icon_name = current_data["icon_list"][1]
        now_icon_location = get_icon_location("night-{}".format(now_icon_name))
    else:
        now_icon_name = current_data["icon_list"][1]
        now_icon_location = get_icon_location(now_icon_name)
    now_icon = weathericons.crop(now_icon_location)
    now_icon = now_icon.resize((70, 70), Image.ANTIALIAS)
    now_icon_x, now_icon_y = 0, 0
    conkywetterstart.paste(now_icon, (now_icon_x, now_icon_y,
                                      now_icon_x + 70, now_icon_y + 70))

    # Jetziges Wetter - Schrift oben links, neben Icon
    for i in range(3):
        draw.text((75, 25), current_data["wetter"], font=font, fill=color)

    # Jetzige Temperatur - Schrift oben rechts
    for i in range(3):
        draw.text((250, 10), current_data["temp"], font=temp_now_font,
                                                   fill=color)

    # Wind - Icon links mittig
    windicon_file = "img/wind.png"
    windicon = Image.open(windicon_file)
    windicon = windicon.resize((25, 25), Image.ANTIALIAS)
    conkywetterstart.paste(windicon, (60, 75))

    # Wind - Schrift links mittig
    for i in range(3):
        draw.text((95, 80), current_data["wind"], font=font, fill=color)

    # Luftfeuchtigkeit - Icon links mittig
    luftfeuchtigkeiticon_file = "img/luftfeuchtigkeit.png"
    luftfeuchtigkeiticon = Image.open(luftfeuchtigkeiticon_file)
    luftfeuchtigkeiticon = luftfeuchtigkeiticon.resize((25, 25),
                                                        Image.ANTIALIAS)
    conkywetterstart.paste(luftfeuchtigkeiticon, (225, 75))

    # Luftfeuchtigkeit - Schrift links mittig
    for i in range(3):
        draw.text((260, 80), current_data["luftfeuchtigkeit"], font=font,
                                                               fill=color)

    # Vorschau Icon Size
    days_icon_size = 40

    # Tag 1 Wochentag - Schrift unten Links
    for i in range(3):
        draw.text((20, 115), forecast_data["day1"]["wochentag"], font=font,
                                                                 fill=color)

    # Tag 1 - Icon unten links
    day1_icon_name = forecast_data["day1"]["icon_list"][1]
    day1_icon_location = get_icon_location(day1_icon_name)
    day1_icon = weathericons.crop(day1_icon_location)
    day1_icon = day1_icon.resize((days_icon_size, days_icon_size),
                                  Image.ANTIALIAS)
    day1_icon_x, day1_icon_y = 25, 135
    conkywetterstart.paste(day1_icon, (day1_icon_x,
                                       day1_icon_y,
                                       day1_icon_x + days_icon_size,
                                       day1_icon_y + days_icon_size))

    # Tag 1 Temperaturen - Schrift unten links
    for i in range(3):
        draw.text((10, 175), forecast_data["day1"]["temp_morning"], font=font,
                                                                    fill=color)
    for i in range(3):
        draw.text((50, 175), "/", font=font, fill=color)
    for i in range(5):
        draw.text((60, 175), forecast_data["day1"]["temp_highest"], font=font,
                                                                    fill=color)

    # Tag 2 Wochentag - Schrift unten Links
    for i in range(3):
        draw.text((165, 115), forecast_data["day2"]["wochentag"], font=font,
                                                                  fill=color)

    # Tag 2 - Icon unten links
    day2_icon_name = forecast_data["day2"]["icon_list"][1]
    day2_icon_location = get_icon_location(day2_icon_name)
    day2_icon = weathericons.crop(day2_icon_location)
    day2_icon = day2_icon.resize((days_icon_size, days_icon_size),
                                  Image.ANTIALIAS)
    day2_icon_x, day2_icon_y = 170, 135
    conkywetterstart.paste(day2_icon, (day2_icon_x,
                                       day2_icon_y,
                                       day2_icon_x + days_icon_size,
                                       day2_icon_y + days_icon_size))

    # Tag 2 Temperaturen - Schrift unten links
    for i in range(3):
        draw.text((155, 175), forecast_data["day2"]["temp_morning"], font=font,
                                                                    fill=color)
    for i in range(3):
        draw.text((195, 175), "/", font=font, fill=color)
    for i in range(5):
        draw.text((205, 175), forecast_data["day2"]["temp_highest"], font=font,
                                                                    fill=color)


    # Tag 3 Wochentag - Schrift unten Links
    for i in range(3):
        draw.text((310, 115), forecast_data["day3"]["wochentag"], font=font,
                                                                  fill=color)

    # Tag 3 - Icon unten links
    day3_icon_name = forecast_data["day3"]["icon_list"][1]
    day3_icon_location = get_icon_location(day3_icon_name)
    day3_icon = weathericons.crop(day3_icon_location)
    day3_icon = day3_icon.resize((days_icon_size, days_icon_size),
                                  Image.ANTIALIAS)
    day3_icon_x, day3_icon_y = 315, 135
    conkywetterstart.paste(day3_icon, (day3_icon_x,
                                       day3_icon_y,
                                       day3_icon_x + days_icon_size,
                                       day3_icon_y + days_icon_size))

    # Tag 3 Temperaturen - Schrift unten links
    for i in range(3):
        draw.text((300, 175), forecast_data["day3"]["temp_morning"], font=font,
                                                                    fill=color)
    for i in range(3):
        draw.text((340, 175), "/", font=font, fill=color)
    for i in range(5):
        draw.text((350, 175), forecast_data["day3"]["temp_highest"], font=font,
                                                                    fill=color)

    conkywetterstart.save(os.path.expanduser("~/.conky/wetter.png"), "PNG")


if __name__ == "__main__":
    """
    In .conkyrc folgendes einfügen:

    ${image ~/.conky/wetter.png -p 0,0 -s 400x200 -f 900}
    ${voffset 160}
    """
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d %H:%M")
    try:
        current_data = get_weather_current()
        sleep(1)
        forecast_data = get_weather_forecast()
        draw_image(current_data, forecast_data)
        write_log("conkywetter ran without errors")
    except Exception as error:
        write_log("ERROR - {}".format(error))
